package com.procyk.maciej.command;

import lejos.nxt.Motor;
import lejos.nxt.NXTRegulatedMotor;

import java.util.LinkedList;

public class PoweredDirectedMotorCommand implements IMotorCommand {

    private LinkedList<IMotorCommand> commands = new LinkedList<>();

    public PoweredDirectedMotorCommand(String command) {
        final int length = command.length();
        final int index = command.indexOf('#');
        if (index == -1) {
            commands.add(parseCommand(command));
        }
        else {
            int beginIndex = 0;
            int endIndex = index;
            while (true) {
                String singleCommand = command.substring(beginIndex, endIndex);
                commands.add(parseCommand(singleCommand));
                beginIndex = endIndex + 1;
                if (beginIndex >= length) {
                    break;
                }
                endIndex = command.indexOf('#', beginIndex);
                if (endIndex == -1) {
                    endIndex = command.length();
                    final String lastCommand = command.substring(beginIndex, endIndex);
                    commands.add(parseCommand(lastCommand));
                    break;
                }
            }
        }
    }

    private IMotorCommand parseCommand(String command) {
        if (command.length() < 5 || command.charAt(0) != 'M'
                || command.charAt(1) != ':' || command.charAt(3) != ':') {
            throw new IllegalArgumentException("Invalid format given");
        }
        final char motor = command.charAt(2);
        final char action = command.charAt(4);
        if (command.length() > 6) {
            final String lastPart = command.substring(6);
            final int value = Integer.parseInt(lastPart, 10);
            return new SinglePowerCommand(motor, action, value);
        }
        else if (command.length() == 5) {
            return new SinglePowerCommand(motor, action);
        }
        else {
            throw new IllegalArgumentException("Invalid format for last part");
        }
    }

    @Override
    public boolean execute() {
        boolean result = true;
        for (IMotorCommand command : commands) {
            result = result && command.execute();
        }
        return result;
    }

    private static class SinglePowerCommand implements IMotorCommand {

        private int power = 0;
        private MotorAction doAction;
        private NXTRegulatedMotor selectedMotor;

        private SinglePowerCommand(char motor, char action) {
            if (motor == 'A') {
                selectedMotor = Motor.A;
            }
            else if (motor == 'B') {
                selectedMotor = Motor.B;
            }
            else if (motor == 'C') {
                selectedMotor = Motor.C;
            }
            else {
                throw new IllegalArgumentException("Invalid motor specified");
            }

            if (action == 'F') {
                doAction = MotorAction.FORWARD;
            }
            else if (action == 'B') {
                doAction = MotorAction.BACKWARD;
            }
            else if (action == 'S') {
                doAction = MotorAction.STOP;
            }
            else {
                throw new IllegalArgumentException("Invalid action specified");
            }
        }

        private SinglePowerCommand(char motor, char action, int power) {
            this(motor, action);
            this.power = power;
        }

        @Override
        public boolean execute() {
            if (this.doAction == MotorAction.STOP) {
                this.selectedMotor.stop();
            }
            else if (this.doAction == MotorAction.FORWARD) {
                this.selectedMotor.setSpeed(this.power);
                this.selectedMotor.forward();
            }
            else if (this.doAction == MotorAction.BACKWARD) {
                this.selectedMotor.setSpeed(this.power);
                this.selectedMotor.backward();
            }
            return true;
        }

        private enum MotorAction {
            FORWARD,
            BACKWARD,
            STOP
        }
    }
}