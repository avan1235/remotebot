package com.procyk.maciej.command;

public interface IMotorCommand {
    /**
     * Execute built command with specified parameters
     * @return true if good command otherwise false
     */
    boolean execute();

    static final String COMMANDS_DELIMITER = "#";
}