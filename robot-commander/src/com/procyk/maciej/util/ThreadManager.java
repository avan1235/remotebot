package com.procyk.maciej.util;

import java.util.*;

public class ThreadManager {
    private final List<Task> tasks;

    
    public ThreadManager() {
        this.tasks = new ArrayList<>();
    }

    public void addTask(Thread thread) {
        this.tasks.add(new Task(thread, false));
    }

    public void addTasks(Thread... threads) {
        for (Thread thread : threads) {
            this.addTask(thread);
        }
    }

    public void stopTasks() {
        for (Task task : tasks) {
            boolean started = task.isStarted();
            Thread thread = task.getThread();
            if (!thread.isInterrupted() && started) {
                thread.interrupt();
                tasks.remove(task);
            }
        }
    }

    public void startTasks() {
        final List<Task> currentTasks = new LinkedList<>(this.tasks);
        for (Task task : currentTasks) {
            boolean started = task.isStarted();
            Thread thread = task.getThread();
            if (!started) {
                thread.start();
                tasks.remove(task);
                tasks.add(new Task(thread, true));
            }
        }
    }

    protected static class Task {
        private final Thread thread;
        private boolean status;

        public Task(Thread thread, Boolean status) {
            this.thread = thread;
            this.status = status;
        }

        public boolean isStarted() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }

        public Thread getThread() {
            return thread;
        }
    }
}
