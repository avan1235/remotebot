package com.procyk.maciej.util;

import lejos.nxt.Button;
import lejos.nxt.ButtonListener;

public class StartListener extends ThreadManager implements ButtonListener {

    public StartListener() { }

    @Override
    public void buttonPressed(Button button) {
        System.out.println("Starting tasks on button release");
    }

    @Override
    public void buttonReleased(Button button) {
        this.startTasks();
    }
}
