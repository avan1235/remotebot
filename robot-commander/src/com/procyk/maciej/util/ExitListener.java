package com.procyk.maciej.util;

import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.comm.NXTConnection;

import java.util.LinkedList;
import java.util.List;

public class ExitListener extends ThreadManager implements ButtonListener {

    private List<NXTConnection> connections;

    public ExitListener() {
        this.connections = new LinkedList<>();
    }

    public void openConnection(NXTConnection connection) {
        this.connections.add(connection);
    }

    @Override
    public void buttonPressed(Button button) {
        exitAction();
    }
    @Override
    public void buttonReleased(Button button) {
        exitAction();
    }

    private void exitAction() {
        this.stopTasks();

        for (NXTConnection connection : connections) {
            connection.close();
        }
        connections.clear();
        System.exit(0);
    }
}
