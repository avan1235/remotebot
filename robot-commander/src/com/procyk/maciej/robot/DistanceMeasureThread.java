package com.procyk.maciej.robot;

import lejos.nxt.I2CPort;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.comm.NXTConnection;

public class DistanceMeasureThread extends Thread {

    private static final int DELAY_TIME = 500;

    private final NXTConnection connection;
    private final I2CPort port;

    public DistanceMeasureThread(I2CPort port, NXTConnection connection) {
        super("DistanceMeasureThread");
        this.port = port;
        this.connection = connection;
    }

    @Override
    public void run() {
        UltrasonicSensor sensor = new UltrasonicSensor(port);
        String data;
        byte[] bytes;
        try {
            while (!isInterrupted()) {
                int measure = sensor.getDistance();
                if (measure != 255) {
                    data = "OUT:" + measure;
                    bytes = data.getBytes();
                    connection.write(bytes, bytes.length);
                    Thread.sleep(DELAY_TIME);
                }
            }
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        } finally {
            sensor.off();
        }
    }
}
