package com.procyk.maciej.robot;

import com.procyk.maciej.command.IMotorCommand;
import com.procyk.maciej.command.PoweredDirectedMotorCommand;
import lejos.nxt.comm.NXTConnection;

import java.util.Arrays;

public class MotorControlThread extends Thread {

    private static final int DELAY_TIME = 1;
    private static final int BUFF_SIZE = 128;

    private final byte[] buffer = new byte[BUFF_SIZE];

    private static final byte[] OK_MSG = "ERR:OK".getBytes();
    private static final byte[] ERROR_MSG = "ERR:ERROR".getBytes();

    private final NXTConnection connection;

    public MotorControlThread(NXTConnection connection) {
        super("MotorControlThread");
        this.connection = connection;
    }

    @Override
    public void run() {
        try {
            int offset;
            System.out.println("Start reading commands");
            while (!isInterrupted()) {
                if (connection.available() > 0) {
                    if ((offset = connection.read(buffer, BUFF_SIZE, false)) < 0) {
                        connection.write(ERROR_MSG, ERROR_MSG.length);
                        continue;
                    }
                    // write on robot what we got from usb connection
                    String commandData = new String(Arrays.copyOf(buffer, offset));
                    System.out.println(commandData);
                    try {
                        IMotorCommand command = new PoweredDirectedMotorCommand(commandData);
                        boolean ok = command.execute();
                        connection.write(ok ? OK_MSG : ERROR_MSG, ok ? OK_MSG.length : ERROR_MSG.length);
                    } catch (Exception e) {
                        connection.write(ERROR_MSG, ERROR_MSG.length);
                        System.out.println("Command error");
                        System.out.println(e.getMessage());
                    }
                }
                Thread.sleep(DELAY_TIME);
            }
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }
    }
}
