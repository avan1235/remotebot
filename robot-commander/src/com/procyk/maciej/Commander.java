package com.procyk.maciej;

import com.procyk.maciej.robot.DistanceMeasureThread;
import com.procyk.maciej.robot.MotorControlThread;
import com.procyk.maciej.util.ExitListener;
import com.procyk.maciej.util.StartListener;
import lejos.nxt.*;
import lejos.nxt.comm.NXTConnection;
import lejos.nxt.comm.USB;

import java.util.Arrays;

public class Commander {

	public static void main(String[] args) throws InterruptedException {

		System.out.println("Waiting for USB connection...");

		final ExitListener exitListener = new ExitListener();
		final StartListener startListener = new StartListener();
		final NXTConnection connection;

		Button.ESCAPE.addButtonListener(exitListener);
		connection = USB.waitForConnection();

		System.out.println("Connected.");
		exitListener.openConnection(connection);

		System.out.println("Click ENTER button to start interpreting commands");

		final Thread reader = new MotorControlThread(connection);
		final Thread distanceMeasure = new DistanceMeasureThread(SensorPort.S4, connection);
		startListener.addTasks(reader, distanceMeasure);
		exitListener.addTasks(reader, distanceMeasure);

		Button.ENTER.addButtonListener(startListener);

		reader.join();
		distanceMeasure.join();
	}
}