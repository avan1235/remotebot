import lejos.pc.comm.*
import java.nio.charset.Charset
import kotlin.concurrent.thread
import kotlin.system.exitProcess

object Main {
    const val SEND_ERR_RESPONSE = false
    const val SEND_OUT_RESPONSE = true
    private val UTF8_CHARSET = Charset.forName("UTF-8")

    @JvmStatic
    fun main(args: Array<String>) {

        val connection = NXTCommFactory.createNXTComm(NXTCommFactory.USB)
        val id = connection.search(null)

        if (connection.open(id[0])) {
            thread {
                while (true) {
                    val line = readLine()
                    connection.write("$line".toByteArray())
                }
            }

            thread {
                while (true) {
                    val bytes = connection.read()
                    val line = bytes.toString(UTF8_CHARSET)
                    parseLine(line)
                }
            }
        }
    }
}

fun parseLine(inputLine: String) {
    val line = inputLine.trim()
    val cutLine = line.substring(line.indexOf(':') + 1)
    if (line.startsWith("OUT:") && Main.SEND_OUT_RESPONSE) {
        System.out.println(cutLine)
    }
    else if (line.startsWith("ERR:") && Main.SEND_ERR_RESPONSE) {
        System.err.println(cutLine)
    }
}
