from threading import Lock
from flask import Flask, request, copy_current_request_context
from flask_socketio import SocketIO, disconnect

from debug import debug
from video_streamer import WebcamVideoStream
from socket import socket, AF_INET, SOCK_DGRAM
from subprocess import Popen as open_process, PIPE
from process_manager import ProcessManager


# CONFIG

video_namespace = '/video'
video_message = 'video_stream'

data_namespace = '/data'
data_message = 'control_data'

control_namespace = '/control'
control_message = 'command_response'

data_field = 'data'

port = 5432


# GLOBAL VARIABLES


async_mode = None
app = Flask(__name__)
socket_io = SocketIO(app, async_mode=async_mode)
driver = ProcessManager(open_process(['java', '-jar', 'driver.jar'], stdin=PIPE, stdout=PIPE, stderr=PIPE)).start()
stream = WebcamVideoStream(src=1, resize_factor=0.3).start()
thread_video, thread_data = None, None
thread_lock_video, thread_lock_data = Lock(), Lock()


def asSingleLine(data):
    return "{}\n".format(data)


# VIDEO NAMESPACE


def video_emitter_thread(max_fps=25):
    delay = 1.0 / float(max_fps)
    while True:
        socket_io.sleep(delay)
        image = stream.read_encoded()
        if image is not None:
            socket_io.emit(video_message, {data_field: image}, namespace=video_namespace)


@socket_io.on('connect', namespace=video_namespace)
def video_connect():
    debug('Client connected video {}'.format(request.sid))
    global thread_video
    with thread_lock_video:
        if thread_video is None:
            thread_video = socket_io.start_background_task(video_emitter_thread)


@socket_io.on('disconnect_request', namespace=video_namespace)
def video_disconnect_request():
    @copy_current_request_context
    def can_disconnect():
        disconnect()


@socket_io.on('disconnect', namespace=video_namespace)
def video_disconnect():
    debug('Client disconnected video {}'.format(request.sid))


# DATA NAMESPACE


def data_emitter_thread(data_delay=0.25):
    while True:
        out = driver.read_out()
        debug("Received from driver: {}\n".format(out))
        socket_io.sleep(data_delay)
        if out is not None:
            socket_io.emit(data_message, {data_field: out}, namespace=data_namespace)
        else:
            debug('Got corrupted data')


@socket_io.on('connect', namespace=data_namespace)
def data_connect():
    debug('Client connected data {}'.format(request.sid))
    global thread_data
    with thread_lock_data:
        if thread_data is None:
            thread_data = socket_io.start_background_task(data_emitter_thread)


@socket_io.on('disconnect_request', namespace=data_namespace)
def data_disconnect_request():
    @copy_current_request_context
    def can_disconnect():
        disconnect()


@socket_io.on('disconnect', namespace=data_namespace)
def data_disconnect():
    debug('Client disconnected data {}'.format(request.sid))


# CONTROL NAMESPACE


@socket_io.on('connect', namespace=control_namespace)
def control_connect():
    debug('Client connected control {}'.format(request.sid))


@socket_io.on('disconnect_request', namespace=control_namespace)
def data_disconnect_request():
    @copy_current_request_context
    def can_disconnect():
        disconnect()


@socket_io.on('disconnect', namespace=control_namespace)
def control_disconnect():
    debug('Client disconnected control {}'.format(request.sid))


@socket_io.on('command', namespace=control_namespace)
def command_message(message):
    debug('Write command to driver {}'.format(message[data_field]))
    line_data = asSingleLine(message[data_field])
    driver.write(line_data)


# APP INSTANCE


def get_ip():
    sck = socket(AF_INET, SOCK_DGRAM)
    try:
        sck.connect(('8.8.8.8', 1))
        ip = sck.getsockname()[0]
    except:
        ip = '127.0.0.1'
    finally:
        sck.close()
    return ip


if __name__ == '__main__':
    socket_io.run(app, debug=False, host=get_ip(), port=port)
