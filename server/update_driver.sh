#!/bin/bash

cd ../driver/ &&
chmod +x ./gradlew &&
./gradlew shadowJar &&
mv ./build/libs/driver.jar ../server/ &&
cd ../server
