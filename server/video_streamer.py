import cv2
import base64
from threading import Thread, Lock
from time import sleep


class WebcamVideoStream:
    def __init__(self, src=1, resize_factor=1.0, sleep_delay_on_run=3):
        self.vc = cv2.VideoCapture(src)
        self.resize_factor = resize_factor
        sleep(sleep_delay_on_run)
        _, self.frame = self.vc.read()
        self.started = False
        self.read_lock = Lock()

    def start(self):
        if self.started:
            return self
        self.started = True
        self.thread = Thread(target=self.update, args=())
        self.thread.start()
        return self

    def update(self):
        while self.started:
            (grabbed, frame) = self.vc.read()
            self.read_lock.acquire()
            _, self.frame = grabbed, frame
            self.read_lock.release()

    def read_encoded(self):
        self.read_lock.acquire()
        if self.frame is None:
            return None
        frame = self.frame.copy()
        self.read_lock.release()
        resized = cv2.resize(frame,
                             (int(frame.shape[1] * self.resize_factor),
                              int(frame.shape[0] * self.resize_factor)),
                             interpolation=cv2.INTER_AREA)
        rotated = cv2.rotate(resized, cv2.ROTATE_180)
        _, buffer = cv2.imencode('.jpg', rotated)
        jpg_as_text = base64.b64encode(buffer)
        return jpg_as_text

    def stop(self):
        self.started = False
        if self.thread.is_alive():
            self.thread.join()

    def __exit__(self, exc_type, exc_value, traceback):
        self.vc.release()
