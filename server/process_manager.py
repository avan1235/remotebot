from threading import Thread, Lock

from debug import debug


class ProcessManager:
    def __init__(self, process):
        self.process = process
        self.out_lock = Lock()
        self.err_lock = Lock()
        self.started = False
        self.err = None
        self.out = None
        self.thread_err = None
        self.thread_out = None

    def start(self):
        if self.started:
            return self
        self.started = True
        self.thread_out = Thread(target=self.update_out, args=())
        self.thread_out.start()
        self.thread_err = Thread(target=self.update_err, args=())
        self.thread_err.start()
        return self

    def update_out(self):
        while self.started:
            line = self.process.stdout.readline()
            debug('Read single out line: {} from driver'.format(line))
            self.out_lock.acquire()
            self.out = line
            self.out_lock.release()

    def update_err(self):
        while self.started:
            line = self.process.stderr.readline()
            debug('Read single err line: {} from driver'.format(line))
            self.err_lock.acquire()
            self.err = line
            self.err_lock.release()

    def read_out(self):
        self.out_lock.acquire()
        out = self.out
        self.out_lock.release()
        return out

    def read_err(self):
        self.err_lock.acquire()
        err = self.err
        self.err_lock.release()
        return err

    def write(self, data):
        self.process.stdin.write(data)

    def stop(self):
        self.started = False
        if self.thread_out.is_alive():
            self.thread_out.join()
        if self.thread_err.is_alive():
            self.thread_err.join()