package com.procyk.maciej.remotebot.robot

import kotlin.math.max
import kotlin.math.min

object RobotSettings {
    const val maxScaledMotorPower = 300

    const val minMotorPower = 10

    var showSensorData = true
    var showVideoData = true
    var isDebugEnabled = true

    val motorsPowerScales = mutableMapOf(
        "A" to 1.0,
        "B" to 1.0,
        "C" to 1.0
    )
    val motorSwapDirection = mutableMapOf(
        "A" to false,
        "B" to false,
        "C" to false
    )
    val motorsNamingPosition = mutableMapOf(
        "left" to "B",
        "right" to "C",
        "special" to "A"
    )

    fun saveSettingsFromPreferences(key: String, value: Any?) = when {
        key == "showSensorData" && value is Boolean -> {
            showSensorData = value
        }
        key == "showVideoData" && value is Boolean -> {
            showVideoData = value
        }
        key == "showDebugData" && value is Boolean -> {
            isDebugEnabled = value
        }
        key == "driveMotors" && value is String -> {
            motorsNamingPosition["left"] = value.substring(0, 1)
            motorsNamingPosition["right"] = value.substring(1, 2)
            motorsNamingPosition["center"] = value.substring(2, 3)
        }
        key == "powerA" && value is Int -> {
            motorsPowerScales["A"] = max(minMotorPower, value).toDouble() / 50.0
        }
        key == "swappedA" && value is Boolean -> {
            motorSwapDirection["A"] = value
        }
        key == "powerB" && value is Int -> {
            motorsPowerScales["B"] = max(minMotorPower, value).toDouble() / 50.0
        }
        key == "swappedB" && value is Boolean -> {
            motorSwapDirection["B"] = value
        }
        key == "powerC" && value is Int -> {
            motorsPowerScales["C"] = max(minMotorPower, value).toDouble() / 50.0
        }
        key == "swappedC" && value is Boolean -> {
            motorSwapDirection["C"] = value
        }
        else -> Unit
    }
}