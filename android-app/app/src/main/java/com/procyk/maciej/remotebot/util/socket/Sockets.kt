package com.procyk.maciej.remotebot.util.socket

import android.app.Activity
import com.procyk.maciej.remotebot.BuildConfig
import com.procyk.maciej.remotebot.R
import com.procyk.maciej.remotebot.util.L
import com.procyk.maciej.remotebot.util.notify
import com.procyk.maciej.remotebot.util.notifySnackbar
import io.socket.client.IO
import io.socket.client.Socket
import org.json.JSONObject
import java.lang.Exception

typealias JSONConsumer = (JSONObject?) -> Unit

interface LocalSocket {
    fun open()
    fun close()

    val onConnect: JSONConsumer
    val onDisconnect: JSONConsumer
    val onMessage: JSONConsumer
}

interface EmitSocket : LocalSocket {
    fun emit(event: String, value: Any)
}

abstract class AbstractSocket(
    val activity: Activity,
    val eventName: String, val namespace: String, val ip: String, val port: Int,
    final override val onMessage: JSONConsumer
) : LocalSocket {

    protected val socket = IO.socket("http://$ip:$port$namespace")

    private var sockedOpened = false

    final override val onConnect: JSONConsumer = produceConsumeNotifier(
        R.string.connect, activity)
    final override val onDisconnect: JSONConsumer = produceConsumeNotifier(
        R.string.disconnect, activity)

    private val events = L(
        eventName to onMessage,
        Socket.EVENT_CONNECT to onConnect,
        Socket.EVENT_DISCONNECT to onDisconnect
    )

    override fun open() {
        sockedOpened = true
        socket.onMany(events)
        socket.open()
    }

    override fun close() {
        if (!sockedOpened) {
            return
        }
        socket.offMany(events)
        socket.close()
    }

    private fun produceConsumeNotifier(messageId: Int, activity: Activity?): JSONConsumer = {
        activity?.runOnUiThread { activity.notifySnackbar(this::class.simpleName + " " + activity.resources.getString(messageId)) }
    }
}

fun Socket.on(event: String, action: (JSONObject?) -> Unit) {
    on(event) { args ->
        val json = try {
            args[0] as? JSONObject
        } catch (_: Exception) { null }
        action(json)
    }
}

fun Socket.off(event: String, action: (JSONObject?) -> Unit) {
    off(event) { args ->
        val json = try {
            args[0] as? JSONObject
        } catch (_: Exception) { null }
        action(json)
    }
}

fun Socket.onMany(handler: List<Pair<String, JSONConsumer>>) = handler.forEach { (e, action) ->
    this.on(e, action)
}

fun Socket.offMany(handler: List<Pair<String, JSONConsumer>>) = handler.forEach { (e, action) ->
    this.off(e, action)
}
