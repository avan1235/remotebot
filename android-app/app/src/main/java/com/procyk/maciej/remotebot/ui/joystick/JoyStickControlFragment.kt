package com.procyk.maciej.remotebot.ui.joystick

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.procyk.maciej.remotebot.R
import com.procyk.maciej.remotebot.robot.MoveVector
import com.procyk.maciej.remotebot.robot.SpecialMotorMoveRobotCommand
import com.procyk.maciej.remotebot.robot.SpecialMotorStopRobotCommand
import com.procyk.maciej.remotebot.robot.SpecialRevMotorMoveRobotCommand
import com.procyk.maciej.remotebot.ui.BaseControlFragment
import com.procyk.maciej.remotebot.util.A
import kotlinx.android.synthetic.main.fragment_joystick_control.dataTextView
import kotlinx.android.synthetic.main.fragment_joystick_control.titleTextView
import kotlinx.android.synthetic.main.fragment_joystick_control.videoView
import kotlinx.android.synthetic.main.motor_joystick.*
import kotlinx.android.synthetic.main.special_button_joystick.*

class JoyStickControlFragment : BaseControlFragment() {

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_joystick_control, container, false)
    }

    override fun getVideoView() = videoView

    override fun getDataTextView() = dataTextView

    override fun getDataTitleTextView(): TextView? = titleTextView

    override fun assignFreshCommands() {
        val specialButtons = A(buttonUp, buttonDown)
        specialButtons.zip(
            A(SpecialMotorMoveRobotCommand(), SpecialRevMotorMoveRobotCommand()))
            .forEach(this::setClickCommandAction)
        specialButtons.zip(
            Array(2) { SpecialMotorStopRobotCommand() })
            .forEach(this::setReleaseCommandAction)

        joystick.setOnMoveListener { angle, strength ->
            val command = MoveVector.toMotorsCommands(angle, strength)
            emitCommand(command)
        }
    }
}
