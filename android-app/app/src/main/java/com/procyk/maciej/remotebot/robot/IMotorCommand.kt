package com.procyk.maciej.remotebot.robot

interface IRobotCommand {
    companion object {
        const val DEFAULT_MOTOR_POWER_PERCENT = 75
        const val MAX_MOTOR_POWER_PERCENT = 100
    }

    fun getRepresentation(): String
}