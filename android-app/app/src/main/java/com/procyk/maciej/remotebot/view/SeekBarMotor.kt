package com.procyk.maciej.remotebot.view

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.procyk.maciej.remotebot.R
import kotlinx.android.synthetic.main.single_seek_bar.view.*

typealias OnSeekBarMotorListener = (Int, Boolean) -> Unit

class SeekBarMotor(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.single_seek_bar, this)
    }

    private lateinit  var text: TextView
    private lateinit var bar: ShiftedSeekBar
    private lateinit var resetButton: Button

    override fun onFinishInflate() {
        super.onFinishInflate()

        text = findViewById(R.id.percentTextView)
        bar = findViewById(R.id.seekBarMotor)
        resetButton = findViewById(R.id.resetButton)
        setOnSeekBarMotorListener(null)
        resetButton.setOnClickListener {
            this.bar.setShiftedProgress(0)
        }
    }

    fun setOnSeekBarMotorListener(listener: OnSeekBarMotorListener?) {
        bar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                this@SeekBarMotor.updateProgressOnView()
                if (listener != null) {
                    listener(this@SeekBarMotor.bar.getShiftedProgress(), fromUser)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit
            override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
        })
    }

    private fun updateProgressOnView() {
        this.text.text = String.format(resources.getString(R.string.seek_bar_motor_format), this.bar.getShiftedProgress())
    }
}
