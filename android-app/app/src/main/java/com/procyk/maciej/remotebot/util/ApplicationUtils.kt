package com.procyk.maciej.remotebot.util

import android.app.Activity
import android.content.Context
import android.content.Context.VIBRATOR_SERVICE
import android.content.Intent
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.View
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import android.widget.Toast.LENGTH_SHORT
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar


inline fun <reified T : Activity> Activity.start(intentAction: Intent.() -> Unit = {}): Unit
        = startActivity(Intent(this, T::class.java).apply(intentAction))


fun Activity.notify(message: String, short: Boolean = true): Unit
        = Toast.makeText(this, message, if (short) LENGTH_SHORT else LENGTH_LONG).show()


fun Fragment.notify(message: String, short: Boolean = true): Unit
        = Toast.makeText(requireContext(), message, if (short) LENGTH_SHORT else LENGTH_LONG).show()

fun Activity.notifySnackbar(message: String, view: View = findViewById(android.R.id.content), short: Boolean = true)
        = Snackbar.make(view, message, if (short) Snackbar.LENGTH_SHORT else Snackbar.LENGTH_LONG).show()

fun Fragment.notifySnackbar(message: String, short: Boolean = true)
        = Snackbar.make(requireView(), message, if (short) Snackbar.LENGTH_SHORT else Snackbar.LENGTH_LONG).show()

fun Activity.runOnUiThread(action: Activity.() -> Unit) = runOnUiThread { this.action()  }

fun Fragment.runOnUiThread(action: Fragment.() -> Unit) = activity?.runOnUiThread { this.action() }

fun Context.vibrate(millis: Int = 150) {
    val vibrator = getSystemService(Vibrator::class.java)
    vibrator?.let {
        if (Build.VERSION.SDK_INT >= 26) {
            it.vibrate(VibrationEffect.createOneShot(millis.toLong(), VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            @Suppress("DEPRECATION")
            it.vibrate(millis.toLong())
        }
    }
}
