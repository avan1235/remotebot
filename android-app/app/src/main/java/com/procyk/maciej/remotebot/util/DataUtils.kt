package com.procyk.maciej.remotebot.util

inline fun <reified T> A(vararg elements: T) = arrayOf(*elements)

inline fun <reified T> L(vararg elements: T) = listOf(*elements)