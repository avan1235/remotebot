package com.procyk.maciej.remotebot.util.socket

import android.app.Activity
import com.procyk.maciej.remotebot.BuildConfig

class VideoSocket(activity: Activity, onMessage: JSONConsumer, ip: String)
    : AbstractSocket(activity,
    BuildConfig.VIDEO_MESSAGE,
    BuildConfig.VIDEO_NAMESPACE, ip,
    BuildConfig.VIDEO_PORT, onMessage)