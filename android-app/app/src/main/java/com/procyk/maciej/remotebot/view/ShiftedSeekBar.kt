package com.procyk.maciej.remotebot.view

import android.content.Context
import android.content.res.Configuration
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.drawable.shapes.OvalShape
import android.util.AttributeSet
import android.widget.SeekBar
import androidx.appcompat.widget.AppCompatSeekBar
import com.procyk.maciej.remotebot.R


class ShiftedSeekBar(context: Context, attrs: AttributeSet?) : AppCompatSeekBar(context, attrs) {

    private val rect = Rect()
    private val paint = Paint()

    companion object {
        const val SHIFT = 50
    }

    init {
        this.setShiftedProgress(0)
    }


    @Synchronized
    override fun onDraw(canvas: Canvas) {
        val seekBarHeight = 60
        val progressValue = getShiftedProgress()
        with(rect) {
            left = 0 + thumbOffset * 3 / 2
            top = height / 2 - seekBarHeight / 2
            right = width - thumbOffset * 3 / 2
            bottom = height / 2 + seekBarHeight / 2
        }
        paint.color = context.getColor(R.color.colorPrimary)
        canvas.drawRect(rect, paint)
        if (progressValue > 0) {
            with(rect) {
                left = width / 2
                top = height / 2 - seekBarHeight / 2
                right = width / 2 + width / 100 * progressValue
                bottom = height / 2 + seekBarHeight / 2
            }
        }
        else if (progressValue < 0) {
            with(rect) {
                left = width / 2 - width / 100 * (-progressValue)
                top = height / 2 - seekBarHeight / 2
                right = width / 2
                bottom = height / 2 + seekBarHeight / 2
            }
        }
        paint.color = context.getColor(R.color.colorAccent)
        if (progressValue != 0) {
            canvas.drawRect(rect, paint)
        }
    }

    fun getShiftedProgress(): Int = super.getProgress() - SHIFT

    fun setShiftedProgress(value: Int) = super.setProgress(value + SHIFT)
}
