package com.procyk.maciej.remotebot

import android.app.Application
import androidx.preference.PreferenceManager
import com.procyk.maciej.remotebot.robot.RobotSettings

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        PreferenceManager.setDefaultValues(applicationContext, R.xml.root_preferences, false);

        val preferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        preferences.all.forEach { pref ->
            RobotSettings.saveSettingsFromPreferences(pref.key, pref.value)
        }
    }
}