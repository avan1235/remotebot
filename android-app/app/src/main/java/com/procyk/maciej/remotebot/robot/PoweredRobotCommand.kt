package com.procyk.maciej.remotebot.robot

import com.procyk.maciej.remotebot.BuildConfig
import com.procyk.maciej.remotebot.robot.IRobotCommand.Companion.DEFAULT_MOTOR_POWER_PERCENT
import com.procyk.maciej.remotebot.robot.IRobotCommand.Companion.MAX_MOTOR_POWER_PERCENT
import com.procyk.maciej.remotebot.robot.RobotSettings.motorSwapDirection
import com.procyk.maciej.remotebot.robot.RobotSettings.motorsNamingPosition
import com.procyk.maciej.remotebot.robot.RobotSettings.motorsPowerScales
import java.lang.IllegalStateException
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

interface PoweredRobotCommand : IRobotCommand {

    fun getRevRepresentation(): String
    fun getMotorPower(): Int
    fun setMotorPower(value: Int)

    fun getMotorPowerRanged(value: Int): Int =
        if (value >= 0) {
            min(value, MAX_MOTOR_POWER_PERCENT)
        } else {
            max(value, -MAX_MOTOR_POWER_PERCENT)
        }
}

open class MoveRobotCommand(private val motorName: () -> String) : PoweredRobotCommand {

    private var motorPower = DEFAULT_MOTOR_POWER_PERCENT

    private fun getMotorPowerScaled(): Int {
        return ((motorsPowerScales[motorName()] ?: 1.0) * getMotorPower()).toInt()
    }

    override fun getRepresentation(): String {
        return getRepresentationHelper()
    }

    override fun getRevRepresentation(): String {
        return getRepresentationHelper(true)
    }

    private fun getRepresentationHelper(reversed: Boolean = false): String {
        val name = motorName()
        val power = getMotorPowerScaled()
        val powerAbs = abs(power)
        val direction = when {
            power >= 0 && motorSwapDirection[name] != true && !reversed -> "F"
            power >= 0 && motorSwapDirection[name] != false && reversed -> "F"
            power < 0 && motorSwapDirection[name] == true && !reversed  -> "F"
            power < 0 && motorSwapDirection[name] == false && reversed  -> "F"
            else -> "B"
        }
        return  "${getRepresentationPrefix()}$direction:$powerAbs"
    }

    fun getRepresentationPrefix(): String {
        val name = motorName()
        return "M:$name:"
    }

    override fun getMotorPower(): Int {
        return ((motorPower.toDouble() / MAX_MOTOR_POWER_PERCENT) * RobotSettings.maxScaledMotorPower).toInt();
    }

    override fun setMotorPower(value: Int) {
        motorPower = getMotorPowerRanged(value)
    }
}

open class StopMoveRobotCommand(motorName: () -> String) : MoveRobotCommand(motorName) {
    override fun getMotorPower(): Int {
        return 0
    }

    override fun getRepresentation(): String {
        return "${getRepresentationPrefix()}S"
    }

    override fun getRevRepresentation(): String {
        return "${getRepresentationPrefix()}S"
    }
}

class SpecialMotorMoveRobotCommand : MoveRobotCommand({ motorsNamingPosition["special"]!! })
class SpecialMotorStopRobotCommand : StopMoveRobotCommand({ motorsNamingPosition["special"]!! })
class SpecialRevMotorMoveRobotCommand : MoveRobotCommand({ motorsNamingPosition["special"]!! }) {
    override fun getRepresentation(): String {
        return super.getRevRepresentation()
    }
    override fun getRevRepresentation(): String {
        return super.getRepresentation()
    }
}

class LeftMotorMoveRobotCommand : MoveRobotCommand({ motorsNamingPosition["left"]!! })
class CenterMotorMoveRobotCommand : MoveRobotCommand({ motorsNamingPosition["special"]!! })
class RightMotorMoveRobotCommand : MoveRobotCommand({ motorsNamingPosition["right"]!! })

sealed class PositionChangeMoveRobotCommand : PoweredRobotCommand {

    protected val motorLeft = MoveRobotCommand { motorsNamingPosition["left"]!! }
    protected val motorRight = MoveRobotCommand { motorsNamingPosition["right"]!! }

    override fun getRepresentation() =
        "${motorLeft.getRepresentation()}${BuildConfig.COMMANDS_DELIMITER}${motorRight.getRepresentation()}"

    override fun getRevRepresentation() =
        "${motorLeft.getRevRepresentation()}${BuildConfig.COMMANDS_DELIMITER}${motorRight.getRevRepresentation()}"

    protected fun setMotorsHelper(motorLeftPower: Int, motorRightPower: Int) {
        motorLeft.setMotorPower(motorLeftPower)
        motorRight.setMotorPower(motorRightPower)
    }

    abstract fun setMotors(motorLeftPower: Int, motorRightPower: Int)

    private var motorsPower = DEFAULT_MOTOR_POWER_PERCENT

    override fun setMotorPower(value: Int) {
        setMotors(value, value)
        motorsPower = getMotorPowerRanged(value)
    }

    override fun getMotorPower(): Int {
        return motorsPower
    }

    init {
        setMotors(DEFAULT_MOTOR_POWER_PERCENT, DEFAULT_MOTOR_POWER_PERCENT)
    }
}

class MoveForwardRobotCommand : PositionChangeMoveRobotCommand() {
    override fun setMotors(motorLeftPower: Int, motorRightPower: Int)
            = setMotorsHelper(motorLeftPower, motorRightPower)
}

class MoveBackwardRobotCommand : PositionChangeMoveRobotCommand() {
    override fun setMotors(motorLeftPower: Int, motorRightPower: Int)
            = setMotorsHelper(-motorLeftPower, -motorRightPower)
}

class MoveLeftRobotCommand : PositionChangeMoveRobotCommand() {
    override fun setMotors(motorLeftPower: Int, motorRightPower: Int)
            = setMotorsHelper(-motorLeftPower, motorRightPower)
}

class MoveRightRobotCommand : PositionChangeMoveRobotCommand() {
    override fun setMotors(motorLeftPower: Int, motorRightPower: Int)
            = setMotorsHelper(motorLeftPower, -motorRightPower)
}

class MoveStopRobotCommand : PositionChangeMoveRobotCommand() {
    override fun setMotors(motorLeftPower: Int, motorRightPower: Int)
            = setMotorsHelper(0, 0)

    override fun getRepresentation(): String {
        return "${motorLeft.getRepresentationPrefix()}S${BuildConfig.COMMANDS_DELIMITER}${motorRight.getRepresentationPrefix()}S"
    }
}

object MoveVector {
    fun toMotorsCommands(directionAngle: Int, strength: Int): IRobotCommand {
        if (directionAngle == 0 && strength == 0) {
            return MoveStopRobotCommand()
        }

        val motorLeftPower = 100 * when (directionAngle) {
            in 0..89 -> 1.0
            in 90..269 -> (180 - directionAngle) / 90.0
            in 270..359 -> -1.0
            else -> throw IllegalStateException("This should not happen")
        }

        val motorRightPower = 100 * when (directionAngle) {
            in 0..89 -> directionAngle / 90.0
            in 90..180 -> 1.0
            in 181..269 -> -1.0
            in 270..359 -> (directionAngle - 360) / 90.0
            else -> throw IllegalStateException("This should not happen")
        }

        val motorLeft = LeftMotorMoveRobotCommand().apply {
            val scaledStrength = (motorLeftPower * strength / 100).toInt()
            setMotorPower(scaledStrength)
        }

        val motorRight = RightMotorMoveRobotCommand().apply {
            val scaledStrength = (motorRightPower * strength / 100).toInt()
            setMotorPower(scaledStrength)
        }

        return object : IRobotCommand {
            override fun getRepresentation()
                    = "${motorLeft.getRepresentation()}${BuildConfig.COMMANDS_DELIMITER}${motorRight.getRepresentation()}"
        }
    }
}