package com.procyk.maciej.remotebot.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.appcompat.widget.AppCompatImageButton

typealias  CustomShapeButtonListener = (CustomShapeButton) -> Unit

class CustomShapeButton : AppCompatImageButton {
    private var releasedListener: CustomShapeButtonListener? = null

    constructor(context: Context) : super(context) {
        initState()
    }
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initState()
    }
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initState()
    }

    fun addOnReleaseListener(listener: CustomShapeButtonListener) {
        releasedListener = listener
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                val touchLocationColor: Int

                val xTouchLocation = event.x.toInt()
                val yTouchLocation = event.y.toInt()

                try {
                    touchLocationColor = drawingCache.getPixel(xTouchLocation, yTouchLocation)
                } catch (e: IllegalArgumentException) {
                    return false
                }
                val clicked = touchLocationColor != Color.TRANSPARENT
                if (clicked) {
                    performClick()
                }
                return clicked
            }

            MotionEvent.ACTION_UP -> {
                releasedListener?.invoke(this)
                return true
            }
        }
        return false
    }
    
    private fun initState() {
        isDrawingCacheEnabled = true
    }
}