package com.procyk.maciej.remotebot.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.core.view.isVisible
import com.procyk.maciej.remotebot.R
import com.procyk.maciej.remotebot.ui.activity.MainActivity.Companion.IP_ADDRESS_KEY
import com.procyk.maciej.remotebot.util.notifySnackbar
import com.procyk.maciej.remotebot.util.start
import com.stealthcopter.networktools.SubnetDevices
import com.stealthcopter.networktools.subnet.Device
import kotlinx.android.synthetic.main.activity_search.*
import java.util.ArrayList

class SearchActivity : AppCompatActivity() {

    private var scanning: Boolean = false

    private val foundDevices = mutableListOf<FoundDevice>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        buttonSearch.setOnClickListener {
            if  (scanning) {
                notifySnackbar("Already scanning...", it)
                return@setOnClickListener
            }
            scanning = true
            progress.visibility = View.VISIBLE
            ipAddressSpinner.visibility = View.INVISIBLE
            buttonSelect.isClickable = false
            buttonSelect.isVisible = false
            notifySnackbar("Scanning...", it)
            Thread { SubnetDevices.fromLocalAddress().setTimeOutMillis(100).findDevices(foundDeviceListener) }.start()
        }
    }

    override fun onResume() {
        super.onResume()
        ipAddressSpinner.visibility = View.INVISIBLE
        progress.visibility = View.INVISIBLE
        buttonSelect.isClickable = false
        buttonSelect.isVisible = false
        scanning = false
    }

    fun Spinner.addDevice(device: Device) {
        foundDevices.add(
            FoundDevice(
                device
            )
        )
        val adapter = ArrayAdapter(
            this@SearchActivity,
            android.R.layout.simple_spinner_item,
            foundDevices.toTypedArray()
        )
        this@SearchActivity.runOnUiThread {
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            this.adapter = adapter
        }
    }

    private class FoundDevice(val ip: String, val mac: String) {
        companion object {
            const val DEFAULT_MAC = "00:00:00:00:00:00"
        }

        constructor(device: Device) : this(device.ip, device.mac ?: DEFAULT_MAC)
        override fun toString(): String = mac
    }

    private val foundDeviceListener = object : SubnetDevices.OnSubnetDeviceFound {
        override fun onFinished(devices: ArrayList<Device>?) {
            scanning = false
            runOnUiThread {
                buttonSelect.isVisible = true
                buttonSelect.isClickable = true
                ipAddressSpinner.visibility = View.VISIBLE
                progress.visibility = View.INVISIBLE
                buttonSelect.setOnClickListener {
                    val device = ipAddressSpinner.selectedItem as? FoundDevice
                    device?.let { start<MainActivity> { putExtra(IP_ADDRESS_KEY, it.ip)  } }
                }
            }
        }

        override fun onDeviceFound(device: Device?) {
            device?.let { ipAddressSpinner.addDevice(it) }
        }
    }
}
