package com.procyk.maciej.remotebot.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64


fun String.base64ToBitmap(): Bitmap = Base64.decode(this, Base64.DEFAULT).let { data ->
    BitmapFactory.decodeByteArray(data, 0, data.size)
}
