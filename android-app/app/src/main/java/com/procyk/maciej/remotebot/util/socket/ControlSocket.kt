package com.procyk.maciej.remotebot.util.socket

import android.app.Activity
import com.procyk.maciej.remotebot.BuildConfig

class ControlSocket(activity: Activity, onMessage: JSONConsumer, ip: String)
    : AbstractSocket(activity,
    BuildConfig.CONTROL_MESSAGE,
    BuildConfig.CONTROL_NAMESPACE, ip,
    BuildConfig.CONTROL_PORT, onMessage),
    EmitSocket {

    override fun emit(event: String, value: Any) {
        socket.emit(event, value)
    }
}