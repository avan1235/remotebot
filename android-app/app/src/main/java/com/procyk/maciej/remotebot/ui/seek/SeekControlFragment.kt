package com.procyk.maciej.remotebot.ui.seek

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.procyk.maciej.remotebot.*
import com.procyk.maciej.remotebot.robot.CenterMotorMoveRobotCommand
import com.procyk.maciej.remotebot.robot.LeftMotorMoveRobotCommand
import com.procyk.maciej.remotebot.robot.RightMotorMoveRobotCommand
import com.procyk.maciej.remotebot.ui.BaseControlFragment
import com.procyk.maciej.remotebot.util.A
import kotlinx.android.synthetic.main.fragment_seek_control.dataTextView
import kotlinx.android.synthetic.main.fragment_seek_control.titleTextView
import kotlinx.android.synthetic.main.fragment_seek_control.videoView
import kotlinx.android.synthetic.main.motor_power_bars.*

class SeekControlFragment : BaseControlFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_seek_control, container, false)
    }

    override fun assignFreshCommands() {
        A(left, center, right)
            .zip(A(LeftMotorMoveRobotCommand(), CenterMotorMoveRobotCommand(), RightMotorMoveRobotCommand()))
            .forEach { (seek, command) ->
                seek.setOnSeekBarMotorListener { progress, _ ->
                    command.setMotorPower(progress * 2) // as the progress is in [0, 50]
                    emitCommand(command)
                }
            }
    }

    override fun getVideoView(): ImageView? = videoView

    override fun getDataTextView(): TextView? = dataTextView

    override fun getDataTitleTextView(): TextView? = titleTextView
}
