package com.procyk.maciej.remotebot.ui.button

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.procyk.maciej.remotebot.*
import com.procyk.maciej.remotebot.robot.*
import com.procyk.maciej.remotebot.ui.BaseControlFragment
import com.procyk.maciej.remotebot.util.A
import kotlinx.android.synthetic.main.fragment_button_control.dataTextView
import kotlinx.android.synthetic.main.fragment_button_control.titleTextView
import kotlinx.android.synthetic.main.fragment_button_control.videoView
import kotlinx.android.synthetic.main.motor_button_move.*
import kotlinx.android.synthetic.main.special_button_move.*

class ButtonControlFragment : BaseControlFragment() {

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_button_control, container, false)
    }

    override fun assignFreshCommands() {
        val moveButtons = A(buttonUp, buttonRight, buttonDown, buttonLeft)
        moveButtons.zip(
            A(MoveForwardRobotCommand(), MoveRightRobotCommand(),
                MoveBackwardRobotCommand(), MoveLeftRobotCommand()))
            .forEach(this::setClickCommandAction)
        moveButtons.zip(
            Array(4) { MoveStopRobotCommand() })
            .forEach(this::setReleaseCommandAction)

        val specialButtons = A(buttonInc, buttonDec)
        specialButtons.zip(
            A(SpecialMotorMoveRobotCommand(), SpecialRevMotorMoveRobotCommand()))
            .forEach(this::setClickCommandAction)
        specialButtons.zip(
            Array(2) { SpecialMotorStopRobotCommand() })
            .forEach(this::setReleaseCommandAction)
    }

    override fun getVideoView(): ImageView? = videoView

    override fun getDataTextView(): TextView? = dataTextView

    override fun getDataTitleTextView(): TextView? = titleTextView
}
