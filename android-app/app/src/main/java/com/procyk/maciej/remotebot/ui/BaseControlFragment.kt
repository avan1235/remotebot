package com.procyk.maciej.remotebot.ui

import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.procyk.maciej.remotebot.BuildConfig
import com.procyk.maciej.remotebot.ui.activity.MainActivity
import com.procyk.maciej.remotebot.R
import com.procyk.maciej.remotebot.robot.PoweredRobotCommand
import com.procyk.maciej.remotebot.robot.RobotSettings
import com.procyk.maciej.remotebot.robot.IRobotCommand
import com.procyk.maciej.remotebot.util.*
import com.procyk.maciej.remotebot.util.socket.*
import com.procyk.maciej.remotebot.view.CustomShapeButton
import org.json.JSONObject
import java.lang.Exception

abstract class BaseControlFragment : Fragment() {

    protected lateinit var videoSocket: LocalSocket
    protected lateinit var dataSocket: DataSocket
    protected lateinit var controlSocket: ControlSocket

    override fun onResume() {
        super.onResume()
        assignFreshCommands()

        getDataTextView()?.let { dataView ->
            dataView.visibility = sensorDataVisibility()
        }
        getDataTitleTextView()?.let { titleView ->
            titleView.visibility = sensorDataVisibility()
        }

        videoSocket = VideoSocket(
            requireActivity(),
            onVideoMessage,
            MainActivity.IP_ADDRESS
        )

        dataSocket = DataSocket(
            requireActivity(),
            onDataMessage,
            MainActivity.IP_ADDRESS
        )

        controlSocket = ControlSocket(
            requireActivity(),
            onControlMessage,
            MainActivity.IP_ADDRESS
        )

        if (RobotSettings.showVideoData) {
            videoSocket.open()
        }
        if (RobotSettings.showSensorData) {
            dataSocket.open()
        }
        controlSocket.open()
    }

    override fun onPause() {
        super.onPause()

        videoSocket.close()
        dataSocket.close()
        controlSocket.close()
    }

    private fun sensorDataVisibility(): Int {
        return if (RobotSettings.showSensorData) View.VISIBLE else View.INVISIBLE
    }

    private val onVideoMessage: JSONConsumer = { json ->
        try {
            val imgData = json!!.getString(BuildConfig.DATA_VALUE_FIELD)
            val img = imgData.base64ToBitmap()
            runOnUiThread { getVideoView()?.setImageBitmap(img) }
        } catch (e: Exception) {
            runOnUiThread { notifySnackbar("Video receive error") }
        }
    }

    private val onDataMessage: JSONConsumer = { json ->
        try {
            val distData = json!!.getInt(BuildConfig.DATA_VALUE_FIELD)
            val formatted =  String.format(this@BaseControlFragment.resources.getString(R.string.distance_placeholder), distData)
            runOnUiThread { getDataTextView()?.text = formatted }
        } catch (e: Exception) {
            runOnUiThread { notifySnackbar("Sensor data receive error") }
        }
    }

    private val onControlMessage: JSONConsumer = { json ->
        try {
            val data = json!!.getString(BuildConfig.DATA_VALUE_FIELD)
            runOnUiThread { notifySnackbar(data) }
        } catch (e: Exception) {
            runOnUiThread { notifySnackbar("Control data Exception occurred") }
        }
    }

    protected fun emitCommand(command: IRobotCommand) {
        val full = command.getRepresentation()
        val representation = createRepresentation(full)
        if (RobotSettings.isDebugEnabled) {
            runOnUiThread { notify(representation) }
        }
        controlSocket.emit(BuildConfig.CONTROL_SEND_MESSAGE,
            JSONObject(representation))
    }

    abstract fun assignFreshCommands()

    private fun createRepresentation(line: String): String
            = "{${BuildConfig.DATA_VALUE_FIELD}:\"$line\"}"

    protected abstract fun getVideoView(): ImageView?

    protected abstract fun getDataTextView(): TextView?

    protected abstract fun getDataTitleTextView(): TextView?

    protected fun setClickCommandAction(buttonCommand: Pair<View, PoweredRobotCommand>)
            = setCommandAction(buttonCommand) { view, command ->
        view.setOnClickListener {
            emitCommand(command)
        }
    }

    protected fun setReleaseCommandAction(buttonCommand: Pair<View, PoweredRobotCommand>)
            = setCommandAction(buttonCommand) { view, command ->
        if (view !is CustomShapeButton) {
            view.setOnTouchListener { v, event ->
                if (event.actionMasked == MotionEvent.ACTION_UP) {
                    emitCommand(command)
                    return@setOnTouchListener true
                }
                v.performClick()
            }
        }
        else {
            view.addOnReleaseListener {
                emitCommand(command)
            }
        }
    }

    private fun setCommandAction(buttonCommand: Pair<View, PoweredRobotCommand>, action: (View, PoweredRobotCommand) -> Unit) {
        val (button, command) = buttonCommand
        action(button, command)
    }
}
