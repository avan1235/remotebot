package com.procyk.maciej.remotebot.util.socket

import android.app.Activity
import com.procyk.maciej.remotebot.BuildConfig

class DataSocket(activity: Activity, onMessage: JSONConsumer, ip: String)
    : AbstractSocket(activity,
    BuildConfig.DATA_MESSAGE,
    BuildConfig.DATA_NAMESPACE, ip,
    BuildConfig.DATA_PORT, onMessage)