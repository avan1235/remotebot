# **RemoteBot project**

## Problem description

Having the standard version of LEGO Mindstorms NXT 2.0 robot user gets the ability to communicate with robot only via USB or Bluetooth connection. This type of connection is really short distance connection so playing with the long distance remotely controlled robots is just not to be used with this toy. This project gives the user ability to easily manage the connection between smartphone and the robot using the special mediator device (in this case OrangePi Zero Plus was used).

### Commands transmission

The mediator is connected to WiFi network and to the robot via the USB connection. Special connector was used to host the connection on external pins of OrangePi. Flask server running on mediator gives the ability to connect to it via SocketIO websocket and send commands to robot by network connection.

### Video streaming

As the additional feature the video streaming socket was used to stream the video frames via websocket connection. It is streamed as the separate namespace of websocket so doesn't have to be used by user.

## Android app control

[![build](https://img.shields.io/gitlab/pipeline/avan1235/remotebot?label=build)](https://gitlab.com/avan1235/remotebot/-/jobs/artifacts/master/raw/RemoteBot.apk?job=build)

Android app is developed to allow the user to easily control his robot motors and get some data from robot sensors. It also show the live video stream from the mediator to get the fun of driving the robot that can be in long distance from user if the network range is big enough.

### Robot control

The app allows to control the motors using different types of controlling:

1. Seek bars controls to select by touch the power and direction of any motor separately. Can be used not only to drive the remote cars but also to control for example some cranes or lifts. The control data is sent to robot as the raw motor control commands in this mode.

2. Button control that has selected two motors as the driving ones and configured their direction and using this configuration allows the user to specify the direction of robot move and then the motors powers is determined by he app and sent to the robot as standard commands.

3. Pad control that also has two motors configured as the driving ones and by this configuration allows the user to control the motors in any angle specified by the user on the pad.

### Video stream

With every type of controlling the robot also the video stream is showed as the separate view and managed in the separate websocket connection. User has the ability to disable the stream by single button tap and the the blank screen in shown in the view.

### Data transmission

Data printed by robot to standard output stream is collected by mediator robot driver and divided by the prefix of the data value.

When data from robot starts with *OUT:* prefix then it is sent to the standard out stream which is the caught by the mediator server app and served as the data websocket transmission.

When data from robot starts with *ERR:* prefix then it is sent to the standard error stream which is the caught by the mediator server app and served as the response for the control messages from app. Using this type of protocol every command send by the user to the robot by external app can get the confirmation of received command and executing command state (for example *done* or *error in execution*).

### App configuration

App gives the user possibility to change the permutation of all motors and setup the touch sensitivity of buttons to scale the power of motors transmitted to robot as the commands. (The data collection port is selected be the user and only single data type is emitted by the robot so this case is not configurable by the user in android app).

### App screenshots

<img alt="Search control" src="https://gitlab.com/avan1235/remotebot/-/raw/master/imgs/search.png" width="200"/>
<img alt="Button control" src="https://gitlab.com/avan1235/remotebot/-/raw/master/imgs/button.png"  width="200"/>
<img alt="Seek control" src="https://gitlab.com/avan1235/remotebot/-/raw/master/imgs/seek.png"  width="200"/>
<img alt="Joystick control" src="https://gitlab.com/avan1235/remotebot/-/raw/master/imgs/joystick.png"  width="200"/>
<img alt="Menu control" src="https://gitlab.com/avan1235/remotebot/-/raw/master/imgs/menu.png"  width="200"/>
<img alt="Settings control" src="https://gitlab.com/avan1235/remotebot/-/raw/master/imgs/settings.png"  width="200"/>

#### App usage video

<a href="https://www.youtube.com/watch?v=66a_eImzNz0" target="_blank"><img src="http://img.youtube.com/vi/66a_eImzNz0/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="480" height="360" border="10" /></a>

